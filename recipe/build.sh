#!/bin/bash

set -e

# Install cargo package into PREFIX
cargo install ${PKG_NAME} --vers ${PKG_VERSION} --root ${PREFIX}
rm -f ${PREFIX}/.crates.toml
